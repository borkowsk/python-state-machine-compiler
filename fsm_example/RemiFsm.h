/******************************************************************************
* @file    fsm
* @author  Stan Borkowski - INRIA
* @date    24/04/2019
*****************************************************************************/
#include "RemiFsmBase.h"

class RemiFsm : public RemiFsmBase
{
public:
    void show_msg(const char *);
    void prepare_connection(void);
    void poll_server(void);
    bool is_connection_speed_ok(void);
    bool is_server_ready(void);
    bool is_locked(void);

};
