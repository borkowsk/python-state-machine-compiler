
#  Copyright (c) 2019. Inria

# Watch out for the commas or their absence at the end of TRANSITIONS dictionaries.

# These are only for readeability.
TRANSITIONS = "TRANSITIONS"
ENTRY_ACTION = "ENTRY_ACTION"
DO_ACTION = "DO_ACTION"
EXIT_ACTION = "EXIT_ACTION"
NEXT_STATE = "NEXT_STATE"
ACTION = "ACTION"
GUARD = "GUARD"
TRIGGER = "TRIGGER"
# --------------------------------


motor_fsm = {
    "IDLE": {  # state name
        ENTRY_ACTION: "led_red()",
        TRANSITIONS: [
            {TRIGGER: "BUTTON_ON", NEXT_STATE: "RUNNING"}
        ]
    },
    "RUNNING": {  # state name
        ENTRY_ACTION: "led_green()",
        TRANSITIONS: [
            {TRIGGER: "BUTTON_OFF", NEXT_STATE: "IDLE"}
        ]
    }
}
