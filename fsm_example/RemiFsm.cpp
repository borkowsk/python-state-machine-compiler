/******************************************************************************
* @file    fsm
* @author  Stan Borkowski - INRIA
* @date    24/04/2019
*****************************************************************************/
#include <iostream>
#include "RemiFsm.h"

void RemiFsm::show_msg(const char *msg)
{
    std::cout << msg << std::endl;
}

void RemiFsm::prepare_connection(void)
{
    show_msg("preparing_connecton");
}

void RemiFsm::poll_server(void)
{
    show_msg("polling server");
}

bool RemiFsm::is_connection_speed_ok(void)
{
    show_msg("is_connection_speed_ok() called");
    return true;
}

bool RemiFsm::is_server_ready(void)
{
    show_msg("is_server_ready() called");
    return true;
}

bool RemiFsm::is_locked(void)
{
    show_msg("is_locked() called");
    return false;
}
