/******************************************************************************
* @file    main.cpp
* @author  Stan Borkowski - INRIA
* @date    24/04/2019
*
* @brief Test application for the Finite state machine C++ generator
*
* Project : main file for fsm_example, derived from Rémi Pincent's C test example
* Contact:  stan.borkowski@inria.fr
*
*****************************************************************************/

/***************************************************************************
* Include Files/
***************************************************************************/
#include <stdio.h>
#include "RemiFsm.h"

/***************************************************************************
* Global Functions Definitions                                              
***************************************************************************/
int main(void)
{
    RemiFsm fsm;
    printf(" *** enter fsm state code generation example ***\n\n");
    fsm.init();
    /** this trigger must not cause any transition */
    fsm.update(RemiFsm::ON_CONNECT);
    /** this trigger must not cause any transition */
    fsm.update(RemiFsm::ON_DISCONNECT);
    for(int i = 0; i < 5; i++){
        /** this update won't call any action */
        fsm.update();
    }
    /** goes in DISCONNECT state */
    fsm.update(RemiFsm::CONNECT_BTN_PRESSED);
    /** this update won't call any action */
    fsm.update();
    /** goes in CONNECT state */
    fsm.update(RemiFsm::ON_CONNECT);
    for(int i = 0; i < 5; i++){
        /** call CONNECTED state action */
        fsm.update();
    }
    /** goes in DISCONNECT state */
    fsm.update(RemiFsm::ON_DISCONNECT);
    /** this update won't call any action */
    fsm.update();
    /** this trigger must not cause any transition */
    fsm.update(RemiFsm::CONNECT_BTN_PRESSED);
    /** this trigger must not cause any transition */
    fsm.update(RemiFsm::ON_DISCONNECT);
    /** goes in CONNECT state */
    fsm.update(RemiFsm::ON_CONNECT);
    /** call CONNECTED state action */
    fsm.update();
    /** goes in IDLE state */
    fsm.update(RemiFsm::SERVER_UNREACHABLE);
    /** this update won't call any action */
    fsm.update();
    printf("\n *** exit fsm state code generation example ***\n\n");

    return 0;
}
