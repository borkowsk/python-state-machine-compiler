
#  Copyright (c) 2019. Inria

# Watch out for the commas or their absence at the end of TRANSITIONS dictionaries.

# These are only for readeability.
TRANSITIONS = "TRANSITIONS"
ENTRY_ACTION = "ENTRY_ACTION"
DO_ACTION = "DO_ACTION"
EXIT_ACTION = "EXIT_ACTION"
NEXT_STATE = "NEXT_STATE"
ACTION = "ACTION"
GUARD = "GUARD"
TRIGGER = "TRIGGER"
RANK = "RANK"
# --------------------------------


# The state machine name must end with '_fsm'.
remi_fsm = {
    'IDLE': {  # state name
        # Each action block can be an empty string or any valid C/C++ code.
        # Multiple code lines must be separated with ';'
        # Quotes must be escaped to form a valid python string.
        ENTRY_ACTION: "show_msg(\"enter IDLE state\")",
        DO_ACTION:    "",  # keys with empty strings as values are not mandatory, but useful for spec readability.
        EXIT_ACTION:  "show_msg(\"exit IDLE state\")",

        # transitions is a list of at least one element.
        # Each element is a dictionary with TRIGGER, NEXT_STATE, ACTION, GUARD keys.
        # Each tuple field can be an empty string or any valid C/C++ code
        TRANSITIONS: [
            {TRIGGER: "CONNECT_BTN_PRESSED", NEXT_STATE: "DISCONNECTED", ACTION: "prepare_connection()", GUARD: ""}
        ]
    },
    'DISCONNECTED': {
        RANK: "A",  # RANK (optional) only influences the FSM layout made by Graphviz.
                    # States having the same label will be placed on the same line in the graph.
                    # The label name does not actually matter: rank "A" or 1 is not higher than rank "B" or 2
        ENTRY_ACTION: "show_msg(\"enter DISCONNECTED state\")",
        DO_ACTION:    "",
        EXIT_ACTION:  "show_msg(\"exit DISCONNECTED state\")",

        TRANSITIONS: [
            {TRIGGER: "ON_CONNECT",
             NEXT_STATE: "CONNECTED",
             ACTION: "",
             GUARD: "is_connection_speed_ok() && is_server_ready()"},
            {TRIGGER: "CONNECT_BTN_PRESSED", NEXT_STATE: "IDLE", ACTION:"", GUARD: "is_locked()"}
        ]
    },
    'CONNECTED': {
        RANK: "A",
        ENTRY_ACTION: "show_msg(\"enter CONNECTED state\"); poll_server()",
        DO_ACTION:    "show_msg(\"alive\")",
        EXIT_ACTION:  "show_msg(\"exit CONNECTED state\")",
        TRANSITIONS: [
            {TRIGGER: "ON_DISCONNECT", NEXT_STATE: "DISCONNECTED"},
            {TRIGGER: "CONNECT_BTN_PRESSED", NEXT_STATE: "IDLE", ACTION: "show_msg(\"bye bye\")"},
            {TRIGGER: "SERVER_UNREACHABLE", NEXT_STATE: "IDLE", ACTION: "show_msg(\"bye bye\")"}
        ]
    },
}

# The optional _fsm_snippets data structure example used by the C++ exporter
# All fields are optional, i.e. you may define only USER_INCLUDES or only public members.
remi_fsm_snippets = {
    'USER_INCLUDES': '<iostream>',  # user includes must be separated by ','
    # Member definitions must be separated by ';'
    # You should provide initial values for member variables, these will be set in the object's init() function.
    'MEMBER_VARIABLES': {
        'public': "int counter = 0; double sum = 0.0",
        'private': "char l = 'a'"
    }
}

status_fsm = {
    "OFF": {  # state name
        DO_ACTION: "led_red(level)",
        TRANSITIONS: [
            {TRIGGER: "BUTTON_ON", NEXT_STATE: "ON", GUARD: 'a > 1'},
            {TRIGGER: "BUTTON_ON", NEXT_STATE: "TEST", GUARD: 'a < 1'}
        ]
    },
    "ON": {  # state name
        DO_ACTION: "led_green()",
        TRANSITIONS: [
            {TRIGGER: "BUTTON_OFF", NEXT_STATE: "OFF"}
        ]
    },
    "TEST": {
        TRANSITIONS: [
            {TRIGGER: "BUTTON_ON", NEXT_STATE: "ON"}
        ]
    }
}
