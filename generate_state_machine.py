#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#  Copyright (c) 2019. Inria

"""
@author  Stan Borkowski - Inria
@date    17/08/2018

@brief FSM code generator script

"""

import argparse
import sys
import os
import importlib

__version__ = "1.0"
__author__ = "Stan Borkowski"
__copyright__ = "Copyright © by INRIA – All rights reserved – 2018"

about = "Generate state machine code and visual representation from a FSM specification." + \
        "Code is generated into current directory. Dot files are written next to spec files."


def check_type(name):
    """Try loading module"""
    sname = os.path.splitext(os.path.basename(name))
    if sname[1] != ".py":
        raise argparse.ArgumentTypeError('fsm_file must be a Python file. See fsm_example/example_fsm.py')
    return name


def has_fsm_suffix(item):
    return item.endswith('_fsm')


def main():
    parser = argparse.ArgumentParser(description=about)
    parser.add_argument('-C', action='store_true', help='export C code')
    parser.add_argument('-p', '--cpp', action='store_true', help='export C++ code')
    parser.add_argument('-d', '--dot', action='store_true', help='export Graphviz .dot file')
    parser.add_argument('--png', action='store_true', help='export Graphviz .dot file and a .png image of that file')
    parser.add_argument('fsm_file', help="file describing the FSM. Currently only .py is supported.",
                        type=check_type, nargs='+')
    args = parser.parse_args(sys.argv[1:])

    # read the contents of the spec files
    fsm_specs = dict()
    processed_fsm_names = []
    for file in args.fsm_file:
        exec(compile(open(file, "rb").read(), file, 'exec'))
        fsm_names = [x for x in locals().keys() if x.endswith('_fsm') and x not in processed_fsm_names]
        processed_fsm_names = processed_fsm_names + fsm_names
        # create a specification dict
        fsm_spec = dict()  # spec_file=os.path.splitext(file)[0])
        for name in fsm_names:
            snippets = locals()[name + '_snippets'] if name + '_snippets' in locals().keys() else dict()
            fsm_spec[name] = {'states': locals()[name], 'snippets': snippets}
        fsm_specs[os.path.splitext(file)[0]] = fsm_spec

    exporters = []

    if args.cpp:
        exporters.append(importlib.import_module('fsm_cpp_export'))
        print('  loaded C++ code generator')

    if args.C:
        exporters.append(importlib.import_module('fsm_c_export'))
        print('  loaded C code generator')

    if args.dot or args.png:
        exporters.append(importlib.import_module('fsm_dot_export'))
        print('  loaded dot file generator')

    for exporter in exporters:
        exporter.export(fsm_specs)

    if args.png:
        print('  using dot executable to generate .png image(s) of the FSM')
        for spec_file, spec_list in fsm_specs.items():
            cmd = 'dot -Tpng ' + spec_file + '.dot' + ' -o ' + spec_file + '.png'
            import subprocess
            subprocess.call(cmd.split())


if __name__ == "__main__":
    main()
