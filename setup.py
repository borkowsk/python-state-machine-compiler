#  Copyright (c) 2019. Inria

from setuptools import setup, find_packages

setup(
    name="fsm_generator",
    version="1.0",
    packages=find_packages(),
    entry_points={
        'console_scripts': [
            'fsm_generator=generate_state_machine:main'
        ]
    }
)