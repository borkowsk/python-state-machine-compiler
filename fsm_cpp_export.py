# -*- coding: utf-8 -*-
'''
@file    fsm_cpp_export.py
@author  Stan Borkowski - INRIA
@date    17/04/2019

 @brief C++ code generation for Finite State Machines

 Project : fsm 
 Contact:  stan.borkowski@inria.fr
'''
#  Copyright (c) 2019. Inria

import re
from datetime import datetime

TRIGGERS_FILE_PREAMBLE = \
    "/******************************************************************************\n" + \
    "* @file    fsm_triggers.h\n" + \
    "* @date    {}\n".format(datetime.now().strftime('%d/%m/%Y')) + \
    "*\n" + \
    "* @brief File generated with fsm_exporter. Do not edit.\n" + \
    "*\n" + \
    "*****************************************************************************/\n" + \
    "#ifndef FSM_TRIGGERS_H\n" + \
    "#define FSM_TRIGGERS_H\n\n"

TRIGGERS_FILE_FOOTER = "#endif /* FSM_TRIGGERS_H */\n"

CODE_PREAMBLE = \
    "/******************************************************************************\n" + \
    "* @file    {fsm_filename}\n" + \
    "* @date    {}\n".format(datetime.now().strftime('%d/%m/%Y')) + \
    "*\n" + \
    "* @brief Generated Finite State Machine C++ implementation\n" + \
    "*\n" + \
    "*****************************************************************************/\n"

DERIVED_HEADER_TEMPLATE = """
#ifndef {fsm_name_upper}_H
#define {fsm_name_upper}_H

#include "{fsm_name}Base.h"

class {fsm_name} : {fsm_name}Base
{{
public:
{methods}
}};

#endif"""

HEADER_TEMPLATE = """
#ifndef GENERATED_{fsm_name_upper}_H
#define GENERATED_{fsm_name_upper}_H

{user_headers}

class {fsm_name}Base {{

public:
    enum Trigger {{
{fsm_triggers}
    }};

    void init();
    void update();
    void update(Trigger);

{public_vars}private:
    enum FsmState {{
{fsm_states}
    }};
    FsmState state;
    // actions guard functions are wrapped in virtual methods
{virtual_methods}{private_vars}}};

#endif"""

BASE_SOURCE_TEMPLATE = """
#include "{fsm_name}Base.h"

void {fsm_name}Base::init()
{{
{variables_init}    state = {fsm_init_state};{init_state_entry_function}
}}


void {fsm_name}Base::update()
{{{in_state_update}}}


void {fsm_name}Base::update(Trigger trigger)
{{{on_trigger_update}}}

"""

IF_STATE_TEMPLATE = """
    if(state == {state}) {{
{action}
        return;
    }}
"""

IF_ON_TRIGGER_TEMPLATE = """
    if(state == {state} && trigger == {trigger}) {{
{exit_action}
{transition_action}
{entry_action}
        state = {next_state};
        return;
    }}
"""

IF_ON_TRIGGER_W_GUARD_TEMPLATE = """
    if(state == {state} && trigger == {trigger} && ( {guard} )) {{
{exit_action}
{transition_action}
{entry_action}
        state = {next_state};
        return;
    }}
"""

USER_HEADERS_START = """
// USER HEADERS START ---------------------------------------------------
// lines between these tags will be copied into the generated super class
"""

USER_HEADERS_END = """
// USER HEADERS END -----------------------------------------------------
"""


def export(specs):
    for spec_file, spec_list in specs.items():
        for name, spec in spec_list.items():
            export_fsm(name, spec)


def export_fsm(name, spec):
    # read user headers from derived class ---------------------------
    user_headers = ""
    header_src = ""
    create_derived_class_header = False
    try:
        header = open(cpp_style(name) + '.h', 'r')
        header_src = header.read()
        header.close()
    except FileNotFoundError:
        create_derived_class_header = True

    # read method declarations in the derived class ------------------
    method_list = re.findall('^ *\w+ \w+\(.*?\);', header_src, re.MULTILINE)
    methods = dict()
    for method in method_list:
        method_spec = method.strip()[:-1]  # removing white spaces and ';' at the end
        methods[mangle(method)] = method_spec

    actions_declaration = ""
    fsm_states = ""
    update_implementation = ""
    update_on_trigger_implementation = ""
    guards_declaration = ""
    init_state = list(spec["states"].keys())[0]
    init_state_action = ""

    # get all triggers -----------------------
    triggers = []
    fsm_triggers = ""
    for state_name, state in spec['states'].items():
        for transition in state['TRANSITIONS']:
            trigger = transition['TRIGGER']
            if trigger not in triggers:
                triggers.append(trigger)
    for i, trigger in enumerate(triggers):
        if i+1 != len(triggers):
            fsm_triggers = fsm_triggers + "        {},\n".format(trigger)
        else:
            fsm_triggers = fsm_triggers + "        {}".format(trigger)

    count = 0
    state_actions = []
    transition_actions = []
    guards = []
    for state, state_spec in spec['states'].items():
        count += 1

        # get fsm init_state_action -------------------------------
        if count == 1:
            if 'ENTRY_ACTION' in state_spec and len(state_spec['ENTRY_ACTION']):
                init_state_action = "\n{}".format(format_code_block(state_spec['ENTRY_ACTION'], "    "))

        # process in-state action in the FSM's update method
        if 'DO_ACTION' in state_spec and len(state_spec['DO_ACTION']):
            code_block = format_code_block(state_spec['DO_ACTION'], "        ")
            update_implementation = update_implementation + IF_STATE_TEMPLATE.format(state=state, action=code_block)

        # get state's exit action
        if 'EXIT_ACTION' in state_spec and len(state_spec['EXIT_ACTION']):
            exit_action = format_code_block(state_spec['EXIT_ACTION'], "        ")
        else:
            exit_action = "        // exit action absent"

        # collect states -----------------------------------------
        if count != len(spec['states']):
            fsm_states = fsm_states + "        {},\n".format(state)
        else:
            fsm_states = fsm_states + "        {}".format(state)

        # collect state actions ---------------------------------
        for action in ('ENTRY_ACTION', 'DO_ACTION', 'EXIT_ACTION'):
            if action in state_spec and len(state_spec[action]) and state_spec[action] not in state_actions:
                state_actions.append(state_spec[action])

        # collect transition actions -----------------------------
        state_transitions = []
        for transition in state_spec['TRANSITIONS']:
            trans_name = transition['TRIGGER']
            next_state = transition['NEXT_STATE']
            action = "// no transition action"
            guard = ""
            if trans_name in state_transitions:
                print('\033[93m'+'WARNING'+'\033[0m ' + trans_name + " has multiple definitions for state " + state +
                      " - unexpected behavior may occur unless you know what you are doing.")
            else:
                state_transitions.append(trans_name)

            if 'ACTION' in transition and len(transition['ACTION']):
                action = transition['ACTION']
                if action not in transition_actions:
                    transition_actions.append(action)

            if 'GUARD' in transition and len(transition['GUARD']):
                guard = transition['GUARD']
                if guard not in guards:
                    guards.append(guard)

            # next state entry action
            if 'ENTRY_ACTION' in spec['states'][next_state] and len(spec['states'][next_state]['ENTRY_ACTION']):
                entry_action = format_code_block(spec['states'][next_state]['ENTRY_ACTION'], "        ")
            else:
                entry_action = "        // next state has no entry action"

            if len(guard):
                implementation = IF_ON_TRIGGER_W_GUARD_TEMPLATE.format(state=state, trigger=trans_name, guard=guard,
                                                                       exit_action=exit_action,
                                                                       transition_action=format_code_block(action, "        "),
                                                                       entry_action=entry_action,
                                                                       next_state=next_state)
            else:
                implementation = IF_ON_TRIGGER_TEMPLATE.format(state=state, trigger=trans_name,
                                                               exit_action=exit_action,
                                                               transition_action=format_code_block(action, "        "),
                                                               entry_action=entry_action,
                                                               next_state=next_state)
            update_on_trigger_implementation = update_on_trigger_implementation + implementation

    # get all functions called in actions and guards ------------------
    functions = dict()
    for action in state_actions + transition_actions:
        for block in action.split(';'):
            for function in re.findall('\w+\(.*\)', block):
                functions[mangle(function)] = function
    for guard in guards:
        for block in re.split('&&|\|\|', guard):
            for function in re.findall('\w+\(.*\)', block):
                functions[mangle(function)] = function

    # find matching methods in the derived class ----------------------
    virtual_methods = ""
    for mangled_function in functions.keys():
        for mangled_method in methods.keys():
            if mangled_function == mangled_method:
                virtual_methods = virtual_methods + "    virtual {} = 0;\n".format(methods[mangled_method])

    # create derived class header if doesn't exist -------------------
    if create_derived_class_header:
        virtual_methods = "    // fsm_generator will use this class' methods as specs for actions and guards\n"
        for method in functions.values():
            virtual_methods = virtual_methods + "    STUB: {};\n".format(method)
        header = open(cpp_style(name) + '.h', 'w')
        header.write(CODE_PREAMBLE.format(fsm_filename=cpp_style(name)))
        header.write(DERIVED_HEADER_TEMPLATE.format(fsm_name_upper=name.upper(), fsm_name=cpp_style(name),
                                                    methods=virtual_methods))
        header.close()
        print("\033[93mWARNING\033[0m created only " + cpp_style(name) + ".h file. Update it and re-run fsm_generator")

    # process user code snippets -------------------------------------
    user_headers = ""
    var_declarations = {'public': "", 'private': ""}
    vars_init = ""
    snippets = spec['snippets']
    if 'USER_INCLUDES' in snippets.keys():
        for header in snippets['USER_INCLUDES'].split(','):
            user_headers = user_headers + '#include ' + header.strip() + '\n'
    if 'MEMBER_VARIABLES' in snippets.keys():
        for scope in var_declarations.keys():
            if scope in snippets['MEMBER_VARIABLES']:
                var_declarations[scope], inits = declarations_and_inits(snippets['MEMBER_VARIABLES'][scope])
                vars_init = vars_init + inits

    # write base class header file -----------------------------------
    header = open(cpp_style(name) + 'Base.h', 'w')
    header.write(CODE_PREAMBLE.format(fsm_filename=cpp_style(name) + 'Base.h'))
    header.write(
        HEADER_TEMPLATE.format(fsm_name_upper=name.upper(), fsm_name=cpp_style(name),
                               user_headers=user_headers,
                               fsm_triggers=fsm_triggers,
                               fsm_states=fsm_states,
                               virtual_methods=virtual_methods,
                               public_vars = var_declarations['public'],
                               private_vars = var_declarations['private']))
    header.close()

    # write base class implementation file ---------------------------
    source = open(cpp_style(name) + 'Base.cpp', 'w')
    source.write(CODE_PREAMBLE.format(fsm_filename=cpp_style(name) + 'Base.cpp'))
    source.write(BASE_SOURCE_TEMPLATE.format(fsm_name=cpp_style(name),
                                             fsm_init_state=init_state,
                                             variables_init=vars_init,
                                             init_state_entry_function=init_state_action,
                                             in_state_update=update_implementation,
                                             on_trigger_update=update_on_trigger_implementation))
    source.close()


def declarations_and_inits(vars_str):
    declarations = ""
    inits = ""
    for block in vars_str.split(';'):
        if block.find('='):
            var_declaration, init_value = block.split('=')
            var_name = var_declaration.split()[1]
            inits = inits + "    {} = {};\n".format(var_name, init_value)
        else:
            var_declaration = block.strip()
        declarations = declarations + "    {};\n".format(var_declaration.strip())
    return declarations, inits


def format_code_block(code, padding):
    block = ""
    for line in re.split(';', code):
        block = block + padding + line.strip() + ';\n'
    return block[:-1]


def mangle(function):
    function_name = re.search('\w+\(.*\)', function).group().split('(')[0]
    args_no = len(re.search('\(.*\)', function).group().split(','))
    return function_name + str(args_no)


# def write_str_between_tags(file_name, new_content, start_tag, end_tag):
#     file_handler = open(file_name, 'a+')
#     file_handler.seek(0)
#     content = file_handler.read()
#     file_handler.close()
#     if content.find(start_tag) == -1:
#         content = content + start_tag + end_tag
#     start_index = content.find(start_tag) + len(start_tag)
#     end_index = content.find(end_tag) + 1
#     file_handler = open(file_name, 'w')
#     file_handler.write(content[:start_index] + new_content + content[end_index:])
#     file_handler.close()
#
#
# def guard_name(guard, state, next_state, trigger):
#     stripped = guard.strip()
#     if stripped.endswith('()') and len(stripped.split('&&')) == 1 and len(stripped.split('||')):
#         return guard
#     return cpp_style(state) + '_To_' + cpp_style(next_state) + '_On_' + cpp_style(trigger) + '_Guard()'
#
#
# def state_action_name(action, action_specifier, state):
#     if action_can_be_virtual(action):
#         return action
#     return cpp_style(state) + '_State' + action_specifier + '()'
#
#
# def transition_action_name(action, state, next_state, trigger, guard):
#     if action_can_be_virtual(action):
#         return action
#     if len(guard):
#         return cpp_style(state) + '_To_' + cpp_style(next_state) + '_On_' + cpp_style(trigger) + '_If_Guard()'
#     return cpp_style(state) + '_To_' + cpp_style(next_state) + '_On_' + cpp_style(trigger) + '()'


def action_can_be_virtual(action):
    if len(action.split(';')) > 1 or action.find('()') == -1:
        return False
    return True


def cpp_style(str):
    out = ""
    for a in str.lower().split('_'):
        out = out + a.capitalize()
    return out
