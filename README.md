# Install
Run the following command in a **Python3** virtual environment: 
```bash
pip install -e .
```
This installs the `fsm_generator` script in your Python environment.
Note that fsm_generator is compatible only with Python 3.

# Usage

To see all options:
```bash
$ fsm_generator -h
```
## C++ code generation

1. Write your state machine as a Python dict data structure following the convention used
in `example_fsm.py` (read the comments in the example)
2. `fsm_generator --cpp example_fsm.py` will generate `RemiFsmBase.cpp RemiFsmBase.h RemiFsm.h StatusFsmBase.c StatusFsmBase.h StatusFsm.h`
files containing the base classes and stub headers for the 2 FSMs specified in `example_fsm.py`.
3. Edit the header files and specify method parameter types and return values
4. Re-run `fsm_generator --cpp example_fsm.py`
5. Implement actions in `RemiFsm.cpp StatusFsm.cpp`

Note that step 3-5 must be done only once.
On each run the generator will read the header files and update the base classes.
The generator never writes to your derived classes.

Besides specifying methods, you can provide specific headers and member variables to be included in the base/super class.
These can be specified in the specification file in a data structure that's name ends with `_fsm_snippets`.

## C code generation
```bash
$ fsm_generator -C [-d] [--png] example_fsm.py [other_fsm_file ...]
```
1. Write your state machine as a Python dict data structure following the convention used
in `example_fsm.py`
2. `fsm_generator -C example_fsm.py` will generate `remi_fsm.c remi_fsm.h status_fsm.c status_fsm.h fsm_triggers.h` file containing the
implementation of the 2 FSMs specified in `example_fsm.py`.
In order to compile these, you need to include the `fsm_c_lib` library in your project.
4. Amend the generated `.c` files and implement actions. Don't worry your modifications won't be
lost if you generate the FSM again, as explained [here](https://github.com/Lahorde/dia_fsm_c_code_generation#user-code)
 

## Graph generation
You can generate a visual representation of your FSM with the `-d` and/or `--png` option.
These will produce `example_fsm.dot` and `example_fsm.png` respectively.

You may fiddle a little with the generated layout by adding a ```'RANK'``` property to two or more states in your FSM.
States having the same rank label will be placed on the same line in the graph.
The label name does not actually matter: rank "A" or 1 is not higher than rank "B" or 2.

The ``'RANK'`` property is optional and does not influence the implementation of the FSM.
 
## Notes on internally triggered state changes
Usually state machines react to external events, but sometimes it necessary to trigger a state change from within the machine.
For instance, if the machine increments a counter and changes to the next state on a threshold.
This is save to call an update with a trigger argument **only inside a "do"** action of a state.
I can't think of a good case for calling it in an "entry" or "exit" action of a state,
and calling it inside a transition's action will result in a lock between the two transitions' target states. 

# Test

Only the `remi_fsm` is compiled and tested.

#### C++
```bash
cd fsm_example
fsm_generator --cpp example_fsm.py
g++ -o fsm_example RemiFsm.cpp RemiFsmBase.cpp main.cpp
if diff fsm_generation_example.log <(./fsm_example) ;then echo "test OK"; else echo "test KO"; fi
```

#### C
```bash
cd fsm_example
fsm_generator -C example_fsm.py
gcc -I "../fsm_c_lib" -o fsm_example remi_fsm.c ../fsm_c_lib/fsm.c main.c
if diff fsm_generation_example.log <(./fsm_example) ;then echo "test OK"; else echo "test KO"; fi
```

 
# Background

State Machines, or [Finite State Machines](https://en.wikipedia.org/wiki/Finite-state_machine) (FSM), are useful in many circumstances,
e.g. when writing automata or programs involving asynchronous events from various sources.
FSM as a programming model is conceptually simple, but implementing non-trivial automata may easily
produce code that is difficult to maintain.
Fortunately, FSMs are very well suited to be formally modeled and implemented through code generation.

This project is a "yet another" take on FMS code generation.
It is strongly inspired by the [State Machine Compiler](http://smc.sourceforge.net),
 and is based on Remi Pincent's [FSM code generator for the Dia editor](https://github.com/Lahorde/dia_fsm_c_code_generation).

## Why make a new generator instead of using SMC or "dia to fsm"?

After testing both SMC and "dia to fsm" I noted that:
1. SMC has a workflow I liked a lot, with the FSM being specified in a simple to read text file,
which is then used to generate the implementation and the graphical view of the FSM.
2. SMC C++ generator produces a nice code following the [State pattern](https://en.wikipedia.org/wiki/State_pattern)
model, but unfortunately I wasn't able to compile it on my Arduino target platform.
While you can disable most of the advanced C++ features using Smc options (-stack 10 -nostreams -noex -cast static_cast),
the code still contained exception statements (try/catch).
3. The C code produced by SMC C generator mimics the State pattern and somewhat less convenient to use.
4. Remi Pincent's "dia to C" generator on the other hand produces very simple C implementation,
that suits well embedded platforms like the Arduino, but only works from the [Dia](http://dia-installer.de)
editor's Python console, which I didn't manage to activate on my Mac.
5. I found it inconvenient to copy-paste 5 lines of code into the Python console every time I
 wanted to generate a new version of my FSM with the "dia to C" generator.
6. Dia is a powerful tool, but the graphics is ugly compared to the Graphviz .dot output from SMC.

This FMS generator is an attempt to combine the best features of both SMC and the dia-based-generator.
I use the C code generator from Remi's Dia-to-C, but use a custom 
FSM specification convention and a `.dot` generator in order to avoid using Dia.
